Editer le fichier styles.css à partir de l'emplacement qui lui est réservé.

Jouer des orientations et de l'ordre des éléments. <header> et <footer> ont une hauteur fixe, les éléments contenus dans <main> doivent s'adapter à l'espace vertical libre.

---

1-Menu horizontal

Présenter le contenu principal à l'horizontale en résolution supérieure à 600px. Le menu a une largeur fixe de 200px.

---

2-Contenu vertical

Les contenus se présentent à la verticale avec un breakpoint à 600px de large. Le menu passe sous l'article.

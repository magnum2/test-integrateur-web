Editer le fichier styles.css à partir de l'emplacement qui lui est réservé.

---

1-Flex

Occuper tout le viewport avec les boîtes basculées à la verticale.

---

2-FlexWrap

Autoriser le retour à la ligne, et placer 4 items par ligne en déterminant leur largeur.

---

3-Nouvelles boîtes

Appliquer des marges aux boîtes, puis centrer le contenu. Ajouter de nouvelles boîtes.

---

4-Extension

Modifier la composition pour étendre la boîte 5 à toute la largeur.

---

5-Breakpoint 1024

Modifier la composition avec un breakpoint à 1024px de large.

---

6-Breakpoint 600

Modifier la composition avec un breakpoint à 600px de large.

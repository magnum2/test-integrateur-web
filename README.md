# Test intégrateur web

Dans ce test nous cherchons à évaluer les compétences suivantes :

- Une bonne connaissance HTML (structure de DOM, balises head, microdonnées) afin d'optimiser notre SEO et d'être le plus conforme possible
- De bonnes connaissances CSS permettant à la fois de mettre en place un layout stable, structuré et responsive
- De bonnes connaissances CSS pour le design (typographie, gradients, media queries, différents sélecteurs CSS...)
- Des connaissances de base avec React. De solides connaissances seront un fort plus.

Dans ce test, vous aurez donc à résoudre plusieurs exercices :

1. A-SEO => DOM et SEO
2. B-Flexboxes
3. C-React
4. D-Design

Pour faire ce test, vous devez commencer par :

1. Forker le repository
2. Mettez votre fork en mode "private" (de sorte que les autres candidats ne puissent pas voir vos réponses)
3. Quand vous avez terminé, invitez-moi sur le repo (utiliser le mail lucas.lbonnet.pro@gmail.com) afin que je puisse consulter vos réponses
4. Nous programmons un rendez-vous pour débriefer le test.

Si vous avez des questions ou des problèmes durant le test qui vous empêchent de répondre à une question ou de continuer, n'hésitez pas à m'envoyer un message à lucas.lbonnet.pro@gmail.com.

Bon courage ! ;)
Lucas

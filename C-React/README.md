## Prérequis (React)

- Installer le paquet "React Responsive Carousel"(https://www.npmjs.com/package/react-responsive-carousel) en utilisant yarn.
  Dans ce test, c'est le composant Slider qui l'appellera.
- Monter le composant Slider à l'intérieur du composant App, dans la div.App
- Dans App.js, importer et passer les datas (data.js) en tant que props au composant Slider. La propriété doit s'appeler "slides"

## Etape 1 (React)

- Importer le composant Carousel, ainsi que Slider.css, dans Slider.js
- Appeler Carousel, de sorte qu'il contienne une liste de div au format suivant :

```jsx
<div className="slide" key={id de la slides}>
  <img src={url de la slide} />
  <div className="overlay">
    <h2 className="overlay_title">{titre de la slide}</h2>
    <p className="overlay_text">{texte de la slide}></p>
  </div>
</div>
```

Le résultat doit correspondre à la capture d'écran ![Étape 1](/test-react/Etape 1.png?raw=true "Preview initial view").

## Etape 2 (CSS)

Arranger l'overlay de la façon indiquée dans la capture d'écran ![Étape 2](/test-react/Etape 2.png?raw=true "Preview initial view")

## Etape 3 (CSS)

Centrer les vignettes de la façon indiquée dans la capture d'écran "Etape 3" et faîtes en sorte que le curseur soit une petite main quand on survole une vignette.
(CSS)
![Étape 3](/test-react/Etape 3.png?raw=true "Preview initial view")

## Etape 4 (React)

Ajouter les options suivantes au carrousel :

- Lecture automatique,
- Intervalle entre chaque slide de 6 secondes
- Boucle infinie
- Largeur des vignettes de 120px
- Points indicateurs centraux désactivés
- Numéro de la page désactivé

## Etape 5 (React)

Ajouter des sélecteurs checkbox, permettant de retirer/de remettre les slides désirés. (cf )

![Étape 5](/test-react/Etape 5.png?raw=true "Preview initial view")

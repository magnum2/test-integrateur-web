A partir du fichier index.html :

---

1- Metadonnées

Mettre en place les metadonnées importantes pour le référencement du site. On doit retrouver :

- Un titre : "Bouygues Telecom"
- Une description : "Bouygues Telecom est un opérateur de télécommunications français, filiale du groupe Bouygues créée en 1994. Il est historiquement le troisième des quatre opérateurs de téléphonie mobile nationaux français, apparu après Orange et SFR et avant Free mobile."
- Le nom de l'auteur : votre nom
- Une icône personalisée pour tous les appareils mobiles ou tablettes (utilisez l'icône images/thumbsup.png)
- Une liste de mots clefs : actualité,science,technologie
- La langue principale du site : française.

---

2- Bonnes pratiques

- Empêcher les moteurs de recherche de suivre les liens présents dans le footer (nofollow).
- Mise en place de la description alternative sur l'image de l'article (alt).
- Mise en place du balisage sémantique afin d'identifier le contenu principal du site avec la balise <main>

---

3- Microdonnées

- Déclarer le <article> comme un article.
- Déclarer le <h2> comme étant le titre.
- Déclarer le <p> comme étant le texte.
- Déclarer le <img> comme étant une photo.

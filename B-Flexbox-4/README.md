Editer le fichier styles.css à partir de l'emplacement qui lui est réservé.

---

1-Menu horizontal

Présenter le menu à l'horizontale. Etendre les surfaces cliquables sur chaque surface colorée.

---

2-Menu vertical

Présenter le menu à la verticale avec un breakpoint à 600px.

---

3-Hover li

Au survol, étirer le <li>.
